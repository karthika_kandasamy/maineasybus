﻿using Data;
using Data.DTO;
using Data.Operations;
using easyBus2.Models;
using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;

namespace easyBus2.Controllers
{
    public class homeController : Controller
    {
        // GET: home
        dataContext dc = new dataContext();
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(adminvalidate adm)
        {
            if (ModelState.IsValid)
            {
                var result = dc.userTable.Where(x => x.userName.Equals(adm.userName) && x.password.Equals(adm.password)).FirstOrDefault();
                //var result = dc.userTable.Where(x => x.userName == adm.userName && (Convert.ToBoolean(String.Compare(x.password, adm.password, StringComparison.Ordinal))).FirstOrDefault();
                //    var result = dc.userTable.Where(x => x.userName==adm.userName && x.password==adm.password).FirstOrDefault();
                // var result = dc.userTable.Where(x => (Convert.ToBoolean(String.Compare(x.userName,adm.userName,false)) && (Convert.ToBoolean(String.Compare(x.password, adm.password, StringComparison.Ordinal)))));


                if (result != null&&result.roleId==1)
                {
                    Session["userName"] = adm.userName;
                    Session["password"] = adm.password;
                    return RedirectToAction("adminpage");
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            else
            {
                return RedirectToAction("Index");
            }
        }
        public ActionResult adminpage()
        {
            return View();
        }

        public ActionResult logOut()
        {
            FormsAuthentication.SignOut();
            Session.Abandon();
            return RedirectToAction("Index", "home");
        }


        public ActionResult userView()
        {
            if (Session["userName"] != null)
            {
                userOperation operation = new userOperation();
                var s = operation.GetAllUser();
                return View(s);
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        public ActionResult createUser()
        {
            if (Session["userName"] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        public ActionResult createUser(userDTO user)
        {
            if (Session["userName"] != null)
            {
                userOperation operation = new userOperation();
                var s = operation.AddUser(user);
                if (s == true)
                    return RedirectToAction("userView");
                else
                    return View();
            }
            else
            {
                return RedirectToAction("Index");
            }
        }
        public ActionResult deleteUser(string userId)
        {
            if (Session["userName"] != null)
            {
                userOperation oper = new userOperation();
                oper.DeleteUser(userId);
                return RedirectToAction("userView");
            }
            else
            {
                return RedirectToAction("Index");
            }
        }
        public ActionResult editUser(string userId)
        {
            if (Session["userName"] != null)
            {
                userOperation oper = new userOperation();
                var s = oper.EditUser(userId);
                return View(s);
            }
            else
            {
                return RedirectToAction("Index");
            }

        }
        [HttpPost]
        public ActionResult editUser(userDTO dto)
        {
            if (Session["userName"] != null)
            {
                userOperation oper = new userOperation();
                var s = oper.SaveUser(dto);
                return RedirectToAction("userView");
            }
            else
            {
                return RedirectToAction("Index");
            }
        }


        public ActionResult busView()
        {
            if (Session["userName"] != null)
            {
                busOperation operation = new busOperation();
                var s = operation.GetBusDetails();
                return View(s);
            }
            else
            {
                return RedirectToAction("Index");
            }
        }
        public ActionResult createBus()
        {
            if (Session["userName"] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Index");
            }
        }
        [HttpPost]
        public ActionResult createBus(busDTO bus)
        {
            if (Session["userName"] != null)
            {
                busOperation operation = new busOperation();
                var s = operation.addBusDetails(bus);
                if (s == true)
                    return RedirectToAction("busView");
                else
                    return View();
            }
            else
            {
                return RedirectToAction("Index");
            }
        }
        public ActionResult deleteBus(int busId)
        {
            if (Session["userName"] != null)
            {
                busOperation oper = new busOperation();
                oper.deleteBusDetails(busId);
                return RedirectToAction("busView");
            }
            else
            {
                return RedirectToAction("Index");
            }
        }
        public ActionResult editBus(int busId)
        {
            if (Session["userName"] != null)
            {
                busOperation oper = new busOperation();
                var s = oper.EditUser(busId);
                return View(s);
            }
            else
            {
                return RedirectToAction("Index");
            }
        }
        [HttpPost]
        public ActionResult editBus(busDTO dto)
        {
            if (Session["userName"] != null)
            {
                busOperation oper = new busOperation();
                var s = oper.updateBusDetails(dto);
                return RedirectToAction("busView");
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        public ActionResult routeView()
        {
            if (Session["userName"] != null)
            {
                routeOperation operation = new routeOperation();
                var s = operation.Display();
                return View(s);
            }
            else
            {
                return RedirectToAction("Index");
            }
        }
        public ActionResult createRoute()
        {
            if (Session["userName"] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Index");
            }
        }
        [HttpPost]
        public ActionResult createRoute(routeDTO route)
        {
            if (Session["userName"] != null)
            {
                if (ModelState.IsValid)
                {
                    routeOperation operation = new routeOperation();
                    var s = operation.Add(route);
                    return RedirectToAction("routeView");
                }
                else
                {
                    return View();
                }
            }
            else
            {
                return RedirectToAction("Index");
            }
        }
        public ActionResult deleteRoute(int routeId)
        {
            if (Session["userName"] != null)
            {
                routeOperation operation = new routeOperation();
                operation.delete(routeId);
                return RedirectToAction("routeView");
            }
            else
            {
                return RedirectToAction("Index");
            }
        }
        public ActionResult editRoute(int routeId)
        {
            if (Session["userName"] != null)
            {
                routeOperation operation = new routeOperation();
                var s = operation.Editroute(routeId);
                return View(s);
            }
            else
            {
                return RedirectToAction("Index");
            }
        }
        [HttpPost]
        public ActionResult editRoute(routeDTO dto)
        {
            if (Session["userName"] != null)
            {
                routeOperation oper = new routeOperation();
                oper.updateroute(dto);
                return RedirectToAction("routeView");
            }
            else
            {
                return RedirectToAction("Index");
            }
        }
        public ActionResult roleView()
        {
            if (Session["userName"] != null)
            {
                roleOperation operation = new roleOperation();
                var s = operation.GetAllrole();
                return View(s);
            }
            else
            {
                return RedirectToAction("Index");
            }
        }
        public ActionResult createRole()
        {
            if (Session["userName"] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Index");
            }
        }
        [HttpPost]
        public ActionResult createRole(roleDTO role)
        {
            if (Session["userName"] != null)
            {
                if (ModelState.IsValid)
                {
                    roleOperation operation = new roleOperation();
                    var s = operation.saveroleTable(role);
                    return RedirectToAction("roleView");
                }
                else
                {
                    return View();
                }
            }
            else
            {
                return RedirectToAction("Index");
            }
        }
        public ActionResult deleteRole(int roleno)
        {
            if (Session["userName"] != null)
            {
                roleOperation operation = new roleOperation();
                operation.deleteroleTable(roleno);
                return RedirectToAction("roleView");
            }
            else
            {
                return RedirectToAction("Index");
            }
        }
        public ActionResult departmentView()
        {
            if (Session["userName"] != null)
            {
                departmentOperation operation = new departmentOperation();
                var s = operation.GetAlldepartment();
                return View(s);
            }
            else
            {
                return RedirectToAction("Index");
            }
        }
        public ActionResult createDepartment()
        {
            if (Session["userName"] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Index");
            }
        }
        [HttpPost]
        public ActionResult createDepartment(departmentDTO department)
        {
            if (Session["userName"] != null)
            {
                if (ModelState.IsValid)
                {
                    departmentOperation operation = new departmentOperation();
                    var s = operation.savedepartmentTable(department);
                    return RedirectToAction("departmentView");
                }
                else
                {
                    return View();
                }
            }
            else
            {
                return RedirectToAction("Index");
            }
        }
        public ActionResult deleteDepartment(int deptno)
        {
            if (Session["userName"] != null)
            {
                departmentOperation operation = new departmentOperation();
                operation.deletedepartmentTable(deptno);
                return RedirectToAction("departmentView");
            }
            else
            {
                return RedirectToAction("Index");
            }
        }


        public ActionResult studentView()
        {
            if (Session["userName"] != null)
            {
                studentOperation operation = new studentOperation();
                var s = operation.viewstudentDetails();
                return View(s);
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        public ActionResult createStudent()
        {
            if (Session["userName"] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        public ActionResult createStudent(studentDTO student)
        {
            if (Session["userName"] != null)
            {
                studentOperation operation = new studentOperation();
                var s = operation.addstudentTable(student);
                if (s == true)
                    return RedirectToAction("studentView");
                else
                    return View();
            }
            else
            {
                return RedirectToAction("Index");
            }

        }
        public ActionResult deleteStudent(string userId)
        {
            if (Session["userName"] != null)
            {
                studentOperation oper = new studentOperation();
                oper.deletestudentDetails(userId);
                return RedirectToAction("studentView");
            }
            else
            {
                return RedirectToAction("Index");
            }
        }
        public ActionResult editStudent(string userId)
        {
            if (Session["userName"] != null)
            {
                studentOperation oper = new studentOperation();
                var s = oper.editstudentdetails(userId);
                return View(s);
            }
            else
            {
                return RedirectToAction("Index");
            }
        }
        [HttpPost]
        public ActionResult editStudent(studentDTO dto)
        {
            if (Session["userName"] != null)
            {
                studentOperation oper = new studentOperation();
                var s = oper.edit(dto);
                return RedirectToAction("studentView");
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        public ActionResult locationView()
        {
            if (Session["userName"] != null)
            {
                locationOperation oper = new locationOperation();
                var s = oper.getlocationDetails();
                return View(s);
            }
            else
            {
                return RedirectToAction("Index");
            }

        }
    }
}