﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.DTO
{
    public class userDTO
    {
        public String userId { get; set; }
        public string userName { get; set; }
        public string firstName { get; set; }
        public string password { get; set; }
        public int roleId { get; set; }
    }
}
