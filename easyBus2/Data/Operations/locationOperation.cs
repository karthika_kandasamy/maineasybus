﻿using Data.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Operations
{
    public class locationOperation
    {
        dataContext dc = new dataContext();

        public IEnumerable<locationDTO> getlocationDetails()
        {
            IEnumerable<locationDTO> location = dc.locationTable.Select(s => new locationDTO()
            {
                locationId = s.locationId,
                busId = s.busId,
                userId = s.userId,
                latitude = s.latitude,
                longitude=s.longitude,
                dateTime=s.dateTime
            }).ToList();

            return location;
        }

    }
}
