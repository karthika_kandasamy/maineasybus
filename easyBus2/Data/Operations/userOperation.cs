﻿using Data.DTO;
using Data.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Operations
{
    public class userOperation
    {

        dataContext ctx = new dataContext();

        public bool SaveUser(userDTO model)
        {
            try
            {
                var user = new userTable()
                {
                    userId = model.userId,
                    userName = model.userName,
                    firstName = model.firstName,
                    password = model.password,
                    roleId = model.roleId
                };
                ctx.userTable.Attach(user);
                ctx.Entry(user).State = EntityState.Modified;
                ctx.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                return false;

            }
        }
        public bool AddUser(userDTO model)
        {
            try
            {
                var user = new userTable()
                {
                    userId = model.userId,
                    userName = model.userName,
                    firstName = model.firstName,
                    password = model.password,
                    roleId = model.roleId
                };
                ctx.userTable.Add(user);
                ctx.SaveChanges();
                return true;
            }
            catch(Exception e)
            {
                return false;
            }
        }
        public IEnumerable<userDTO> GetAllUser()
        {
            IEnumerable<userDTO> user = ctx.userTable.Select(s => new userDTO()
            {
                userId = s.userId,
                userName = s.userName,
                firstName = s.firstName,
                password = s.password,
                roleId = s.roleId
            }).ToList();

            return user;
        }
        public bool DeleteUser(string userId)
        {
            try
            {
                var user = ctx.userTable.FirstOrDefault(m => m.userId == userId);
                ctx.userTable.Remove(user);
                ctx.SaveChanges();
                return true;
            }
            catch(Exception e)
            {
                return false;
            }
        }
        public userDTO EditUser(string userId)
        {
            try
            {
                var user = ctx.userTable.FirstOrDefault(m => m.userId == userId);
                var useredit = new userDTO()
                {
                    userId = user.userId,
                    userName = user.userName,
                    firstName = user.firstName,
                    password = user.password,
                    roleId = user.roleId
                };
                return useredit;
            }
            catch(Exception e)
            {
                return null;
            }

        }
    }
}
