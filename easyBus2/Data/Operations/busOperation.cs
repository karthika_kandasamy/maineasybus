﻿using Data.DTO;
using Data.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Operations
{
    public class busOperation
    {
        dataContext bc = new dataContext();

        public bool addBusDetails(busDTO savemodel)
        {
            try
            {
                busTable record = new busTable() { busId = savemodel.busId, busNo = savemodel.busNo, routeId = savemodel.routeId };
                bc.busTable.Add(record);
                bc.SaveChanges();
                return true;
            }
            catch(Exception err) {
                return false;
            }
        }

        public bool updateBusDetails(busDTO savemodel)
        {
            try
            {
                busTable record = new busTable() { busId = savemodel.busId, busNo = savemodel.busNo, routeId = savemodel.routeId };
            bc.busTable.Attach(record);
            bc.Entry(record).State = EntityState.Modified;
            bc.SaveChanges();
            return true;
            }
            catch (Exception err)
            {
                return false;
            }
        }

        public IEnumerable<busDTO> GetBusDetails()
        {
            IEnumerable<busDTO> buses = bc.busTable.Select(s => new busDTO()
            {
                busId = s.busId,
                busNo = s.busNo,
                routeId = s.routeId
            }).ToList();

            return buses;
        }

        public bool deleteBusDetails(int busId)
        {
            var deleterecord = bc.busTable.FirstOrDefault(x => x.busId == busId);
            bc.busTable.Remove(deleterecord);
            bc.SaveChanges();
            return true;
        }

        public busDTO EditUser(int busId)
        {
            try
            {
                var bus = bc.busTable.FirstOrDefault(m => m.busId == busId);
                var editBus = new busDTO()
                {
                    busId = bus.busId,
                    busNo = bus.busNo,
                    routeId = bus.routeId
                };
                return editBus;
            }
            catch(Exception err)
            {
                return null;
            }

        }


    }
}
