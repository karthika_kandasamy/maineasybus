﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.DTO
{
    public class busDTO
    {
        public int busId { get; set; }
        public string busNo { get; set; }
        public int routeId { get; set; }
    }
}
